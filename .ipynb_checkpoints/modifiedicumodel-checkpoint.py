class Person(object):
    def __init__(self):
        self.status = "Susceptible"
        self.days_sick = 0
        self.recovery_rate=5
        self.sickness_rate = 10
        self.critical_care_rate=10
        self.days_for_death=7
        self.death_rate=2
        self.Interaction = []
        self.masked = False
        self.vaccine = False
        self.critical=False
        self.icu=False
        
    def vaccinate(self,took_vaccine):
        self.vaccine=took_vaccine
        if self.masked and self.vaccine:
            self.set_critical_rate(self.critical_care_rate/3)
            self.set_sickness_rate(self.sickness_rate/6)
            self.set_recovery_rate(self.recovery_rate/2)
            self.set_death_rate(self.death_rate/4)
        elif self.vaccine:
            self.set_critical_rate(self.critical_care_rate/3)
            self.set_sickness_rate(self.sickness_rate/3)
            self.set_recovery_rate(self.recovery_rate/2)
            self.set_death_rate(self.death_rate/4)
            
    def maskUp(self,wears_mask):
        self.masked=wears_mask
        if self.masked:
            self.set_sickness_rate(self.sickness_rate/2)
            
    def set_death_rate(self,new_rate):
        self.death_rate=new_rate
    
    def set_sickness_rate(self, new_rate):
        self.sickness_rate = new_rate
    
    def set_recovery_rate(self,new_rate):
        self.recovery_rate=new_rate
        
    def set_critical_rate(self, new_rate):
        self.critical_care_rate = new_rate
    
    def infect(self, number_of_days_sick):
        self.status = "Infected"
        self.days_sick = number_of_days_sick
    
    def recover(self):
        self.status = "Recovered"
        self.days_sick = 0
        self.set_sickness_rate(self.sickness_rate/4)
        self.set_critical_rate(self.critical_care_rate/2)
        self.set_recovery_rate(self.recovery_rate/2)
        self.critical=False
        self.icu=False
    
    def update(self):
        if (self.status == "Susceptible"):
            self.roll_for_infection()
            
        elif (self.status == "Infected"):
            self.days_sick -= 1
            # self.roll_for_critical()
            if self.critical and not self.icu:
                self.days_sick+=1
            elif self.critical and self.icu:
                self.days_sick-=2
    
    def update_days_sick(self):
        available_icu=0.
        if (self.status == "Infected"):            
            self.days_sick -= 1
            # self.roll_for_critical()
            if self.critical and not self.icu:
                self.days_sick+=1
                available_icu=self.russian_roullette()
            elif self.critical and self.icu:
                self.days_sick-=2
                available_icu=self.russian_roullette()

            if (self.days_sick <= 0):
                if self.icu:
                    available_icu=1.
                    self.recover()
                else:
                    self.recover()
            # if self.days_sick>self.days_for_death:
            #     self.status="Dead"
            #     self.icu=False
            #     self.critical=False
        return available_icu
            
    def critical_care(self,available):
        if self.critical and available:
            self.icu=True
        else:
            pass
    
    def roll_for_infection(self):
        is_sick = (random.randint(0,101) < self.sickness_rate)
        if (is_sick):
            self.infect(self.recovery_rate-1)
            
    def roll_for_critical(self):
        if self.status=="Infected":
            critical=(random.randint(0,101)<self.critical_care_rate)
            if critical:
                self.critical=True
                self.days_sick+=int(self.recovery_rate/2)
                
    def russian_roullette(self):
        available_icu=0.
        if self.status=="Infected" and self.critical and self.days_sick>self.days_for_death:
            dead=(random.randint(0,101)<self.death_rate)
            if dead:
                if self.icu:
                    available_icu=1.
                else:
                    pass
                self.critical=False
                self.status="Dead"
                self.icu=False
        return available_icu

class Population:
    def __init__(self, number=100, interaction=10, percentage_masked=0, percentage_vaccinated=0, vaccines_per_day=0,icu=0):
        self.People = []
        self.number = number
        self.interaction = interaction
        self.percentage_masked = percentage_masked
        self.percentage_vaccinated=percentage_vaccinated
        self.vaccines_per_day=vaccines_per_day
        self.icu=icu
        self.available_icu=icu
        
        patient_Zero = Person()
        patient_Zero.infect(patient_Zero.recovery_rate)
        
        self.People.append(patient_Zero)
        
        for i in range(1,number):
            p = Person()
            self.People.append(p)
        
    def StartVaccinating(self):
            if self.number_vaccinated()<self.percentage_vaccinated*self.number/100:
                for p in random.sample(population=self.not_dead(),k=int(self.vaccines_per_day)):
                    p.vaccinate(True)
                # print("Vacuné",self.vaccines_per_day,"personas")
    
    def StartMasking(self):
        for p in random.sample(population=self.not_dead(),k=int(self.percentage_masked*self.number/100)):
            p.maskUp(True)
            
    def number_of_sick(self):
        sick_Count = 0
        for j in self.People:
            if (j.status == "Infected"):
                sick_Count += 1
                
        return sick_Count
    
    def not_dead(self):
        not_dead=[]
        for j in self.People:
            if j.status!="Dead":
                not_dead.append(j)
        return not_dead
    
    def number_masked(self):
        masked_count = 0
        for j in self.People:
            if (j.masked):
                masked_Count += 1
        return masked_Count
    
    def number_of_recovered(self):
        recovered_Count = 0
        for j in self.People:
            if (j.status == "Recovered"):
                recovered_Count += 1
        
        return recovered_Count
    
    def number_of_susceptible(self):
        susceptible_Count = 0
        for j in self.People:
            if (j.status == "Susceptible"):
                susceptible_Count += 1
                
        return susceptible_Count
    
    def number_masked(self):
        masked_Count = 0
        for j in self.People:
            if (j.masked):
                masked_Count += 1
        
        return masked_Count
    
    def number_masked_sick(self):
        masked_SickCount = 0
        for j in self.People:
            if (j.masked and j.status == "Infected"):
                masked_SickCount = masked_SickCount + 1
        
        return masked_SickCount
    
    def number_vaccinated(self):
        vaccine = 0
        for j in self.People:
            if (j.vaccine):
                vaccine += 1
        return vaccine
    
    def number_vaccinated_sick(self):
        vaccine_SickCount = 0
        for j in self.People:
            if (j.vaccine and j.status == "Infected"):
                vaccine_SickCount += 1
        
        return vaccine_SickCount
    
    def number_maskedccinated_sick(self):
        maskccine_SickCount = 0
        for j in self.People:
            if (j.vaccine and j.masked and j.status == "Infected"):
                maskccine_SickCount+=1
        
        return maskccine_SickCount
    
    def number_critical(self):
        crit=0
        for i in self.People:
            if i.critical:
                crit+=1
        return crit
    
    def number_used_icu(self):
        crit=0
        for i in self.People:
            if i.icu:
                crit+=1
        return crit    
    
    def number_dead(self):
        crit=0
        for i in self.People:
            if i.status=="Dead":
                crit+=1
        return crit
    
    ## Here's the brains of the Population class (and our code)
    ## the update will be ran everyday (meaning, every iteration of our while loop)
    ## build interaction tables (who interacted with whom)
    ## if marked sick they must roll to see if (one per interaction)
    ## if I interacted with 10 people, each of them must roll and vice versa if they
    ## they were marked as infected
    def update(self):
        
        might_Get_Sick = []
        not_icu=[]
        
        for p in range(len(self.People)):
            person=self.People[p]
            if not person.icu or person.status!="Dead" or not person.critical:
                not_icu.append(p)
            else:
                pass
        
        for p in not_icu:
            person=self.People[p]
            #clear out your interactions per day
            person.Interaction = []
            
            ##These are the n number of people that you interact with
            # for i in range(0, self.interaction):
            r = random.sample(population=not_icu,k=self.interaction)
            person.Interaction=r
                
            ##if "person" is Infected, every single person they interacted with might get sick
            if (person.status == "Infected"):
                k=person.update_days_sick()
                self.available_icu+=k
                # person.roll_for_critical()
                
                if person.critical and not person.icu and self.available_icu>0:
                    person.critical_care(True)
                    self.available_icu-=1
                elif person.critical and self.available_icu==0:
                    person.critical_care(False)
                    
                elif person.critical and person.icu:
                    pass
                
                for m in person.Interaction:
                    if (self.People[m].status == "Susceptible" or self.People[m].status=="Recovered"): # or self.People[m].status=="Recovered"): #only susceptible people can get sick
                        might_Get_Sick.append(self.People[m])
            
            ##if "person" is susceptible and they interact w/ someone who is "infected" 
            ##person gets added to the "might get sick"
            if (person.status == "Susceptible"): # or person.status == "Recovered"):
                for m in person.Interaction:
                    if (self.People[m].status == "Infected" or person.status=="Recovered"):
                        might_Get_Sick.append(person)
            
        ##Now! We're going to roll through our "Might Get Sick" and they each roll a die
        ##this is only a list of susceptible people
        for p in might_Get_Sick:
            p.roll_for_infection()
            p.roll_for_critical()
            
def start(printing):
    # Population( size_of_population, number_of_interactions, percentage_wearing_masks)
    myCommunity = Population(number=int(4e4), interaction=5, percentage_masked=80., percentage_vaccinated=70.,vaccines_per_day=0.01*4e4,icu=66)
    day = 0
    my_plot = []
    number_sick = myCommunity.number_of_sick()
    while (number_sick > 0):
        if myCommunity.number_of_sick()>0.1*myCommunity.number and myCommunity.number_masked()<myCommunity.percentage_masked*myCommunity.number/100:
            myCommunity.StartMasking()
        if myCommunity.number_of_sick()>0.25*myCommunity.number or myCommunity.number_vaccinated()>myCommunity.vaccines_per_day:
            myCommunity.StartVaccinating()
        number_sick = myCommunity.number_of_sick()
        number_recovered = myCommunity.number_of_recovered()
        number_masked = myCommunity.number_masked()
        number_masked_sick = myCommunity.number_masked_sick()
        number_vaccinated_sick=myCommunity.number_vaccinated_sick()
        number_maskedccinated_sick=myCommunity.number_maskedccinated_sick()
        number_susceptible = myCommunity.number_of_susceptible()
        number_critical=myCommunity.number_critical()
        number_icu=myCommunity.number_used_icu()
        number_dead=myCommunity.number_dead()

        my_plot.append([day, number_susceptible, number_sick, number_recovered, number_masked_sick, number_vaccinated_sick, number_maskedccinated_sick, number_critical, number_icu, number_dead])
        if printing:
            print ("day:", day, "Number of sick:", number_sick,"Dead:",number_dead,"Vaccination:",myCommunity.number_vaccinated(),"Masked:",myCommunity.number_masked())

        day = day+1
        myCommunity.update()
    
    if printing:
        #print ("day:", day, "Number of sick:", myCommunity.number_of_sick())
        print ("Number recovered:", myCommunity.number_of_recovered())
        print ("Number susceptible:", myCommunity.number_of_susceptible())
    df = pd.DataFrame(my_plot, columns=['day',"number_susceptible",'number_sick', 'number_recovered',"number_masked_sick","number_vaccinated_sick","number_maskedccinated_sick","number_critical","number_icu","number_dead"])
    return df